import React, { useEffect, useReducer, useRef, useState } from "react"
import Button from "@material-ui/core/Button"
import Grid from "@material-ui/core/Grid"
import CssBaseline from "@material-ui/core/CssBaseline"
import { makeStyles } from "@material-ui/core/styles"
import { ThemeProvider, createMuiTheme } from "@material-ui/core/styles"

import Api from "../api"
import MessageColumn from "./MessageColumn"
import MessageSnackbar from "./MessageSnackbar"

const useStyles = makeStyles({
  button: {
    backgroundColor: "#00E2C4",
    fontWeight: "800",
    marginLeft: "6px",
    "&:hover": {
      backgroundColor: "#00E2C4",
    },
  },
})

const theme = createMuiTheme({
  typography: {
    body1: {
      fontSize: "0.7rem",
    },
  },
  overrides: {
    MuiButton: {
      root: {
        fontSize: "0.6rem",
      },
    },
  },
})

export default function MessageGrid() {
  const classes = useStyles()
  const [errors, setErrors] = useState([])
  const [warnings, setWarnings] = useState([])
  const [infos, setInfos] = useState([])
  const [snackbar, setSnackbar] = useState()
  const [, forceUpdate] = useReducer((x) => x + 1, 0)
  const api = useRef()

  useEffect(() => {
    api.current = new Api({
      messageCallback: (message) => {
        function prependMessage(m) {
          return [message, ...m.slice()]
        }
        switch (message.priority) {
          case 1:
            setErrors(prependMessage)
            setSnackbar(message.message)
            break
          case 2:
            setWarnings(prependMessage)
            break
          case 3:
            setInfos(prependMessage)
            break
          default:
            break
        }
      },
    })
    api.current.start()
  }, [])

  function handleStartStopClick() {
    if (isApiStarted) {
      api.current.stop()
    } else {
      api.current.start()
    }
    forceUpdate()
  }

  function clearAllMessages() {
    setErrors([])
    setWarnings([])
    setInfos([])
  }
  function clearMessage(messages, setMessage, index) {
    let newMessages = messages.slice()
    newMessages.splice(index, 1)
    setMessage(newMessages)
  }
  function clearError(index) {
    clearMessage(errors, setErrors, index)
  }
  function clearWarning(index) {
    clearMessage(warnings, setWarnings, index)
  }
  function clearInfo(index) {
    clearMessage(infos, setInfos, index)
  }

  const isApiStarted = api.current && api.current.isStarted()
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <MessageSnackbar message={snackbar} setMessage={setSnackbar} />
      <hr style={{ marginTop: "80px" }} />
      <Grid container direction="column">
        <Grid container item justify="center">
          <Button
            className={classes.button}
            variant="contained"
            onClick={handleStartStopClick}
          >
            {isApiStarted ? "Stop" : "Start"}
          </Button>
          <Button
            className={classes.button}
            variant="contained"
            onClick={clearAllMessages}
          >
            Clear
          </Button>
        </Grid>
        <Grid container item direction="row" justify="center" spacing={1}>
          <MessageColumn
            heading="Error Type 1"
            messages={errors}
            clearMessage={clearError}
          />
          <MessageColumn
            heading="Warning Type 2"
            messages={warnings}
            clearMessage={clearWarning}
          />
          <MessageColumn
            heading="Info Type 3"
            messages={infos}
            clearMessage={clearInfo}
          />
        </Grid>
      </Grid>
    </ThemeProvider>
  )
}
