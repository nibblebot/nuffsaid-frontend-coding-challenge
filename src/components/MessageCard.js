import React from "react"
import Grid from "@material-ui/core/Grid"
import Typography from "@material-ui/core/Typography"
import Button from "@material-ui/core/Button"
import Box from "@material-ui/core/Box"
import Card from "@material-ui/core/Card"
import { makeStyles } from "@material-ui/core/styles"

const useStyles = makeStyles({
  card: {
    width: "100%",
    height: "100%",
  },
  error: {
    backgroundColor: "#F56236",
  },
  warning: {
    backgroundColor: "#FCE788",
  },
  info: {
    backgroundColor: "#88FCA3",
  },
})

const priorityMap = {
  1: "error",
  2: "warning",
  3: "info",
}

export default function MessageCard({ message, onClick }) {
  const classes = useStyles()
  const priorityClass = priorityMap[message.priority]

  return (
    <Card className={`${classes[priorityClass]} ${classes.card}`}>
      <Box p={1}>
        <Grid container>
          <Grid item xs>
            <Typography>{message.message}</Typography>
          </Grid>
          <Grid item xs={3}>
            <Button onClick={onClick}>CLEAR</Button>
          </Grid>
        </Grid>
      </Box>
    </Card>
  )
}
