import React from "react"
import Grid from "@material-ui/core/Grid"
import Typography from "@material-ui/core/Typography"

import MessageCard from "./MessageCard"

export default function MessageColumn({ heading, messages, clearMessage }) {
  return (
    <Grid container item direction="column" xs={12} sm={3} spacing={1}>
      <Grid container item direction="column">
        <Grid item>
          <Typography variant="h6">{heading}</Typography>
        </Grid>
        <Grid item>
          <Typography variant="subtitle1">Count {messages.length}</Typography>
        </Grid>
      </Grid>
      <Grid container item direction="column" spacing={1}>
        {messages.map((message, index) => (
          <Grid item key={message.id}>
            <MessageCard
              message={message}
              onClick={() => clearMessage(index)}
            />
          </Grid>
        ))}
      </Grid>
    </Grid>
  )
}
