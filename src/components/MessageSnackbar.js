import React from "react"
import Snackbar from "@material-ui/core/Snackbar"
import Alert from "@material-ui/lab/Alert"
import { makeStyles } from "@material-ui/core/styles"

const useStyles = makeStyles({
  root: {
    color: "black",
  },
})

function MessageSnackbar({ message, setMessage }) {
  const classes = useStyles()
  function handleCloseSnackbar(event, reason) {
    if (reason === "clickaway") {
      return
    }
    setMessage()
  }

  return (
    <Snackbar
      open={!!message}
      anchorOrigin={{ vertical: "top", horizontal: "center" }}
      autoHideDuration={2000}
      onClose={handleCloseSnackbar}
    >
      <Alert
        variant="filled"
        severity="error"
        onClose={handleCloseSnackbar}
        classes={{
          root: classes.root,
        }}
      >
        {message}
      </Alert>
    </Snackbar>
  )
}

export default MessageSnackbar
