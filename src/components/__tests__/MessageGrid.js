import React from "react"
import { fireEvent, render } from "@testing-library/react"
import MessageGrid from "../MessageGrid"

it("start/stop/clear/clear all buttons", () => {
  const { getByText, getAllByText } = render(<MessageGrid />)

  // visible stop button
  expect(getByText(/stop/i)).toBeVisible()

  // at least one message
  let clearableNodes = getAllByText(/clear/i)
  expect(clearableNodes.length).toBeGreaterThan(1)

  // visible clear all button, clear button
  expect(clearableNodes[0]).toBeVisible()
  expect(clearableNodes[1]).toBeVisible()

  // click stop, visible start button
  fireEvent.click(getByText(/stop/i))
  expect(getByText(/start/i)).toBeVisible()

  // click clear all
  fireEvent.click(clearableNodes[0])
  expect(getAllByText(/clear/i).length).toEqual(1)

  // at least one message
  fireEvent.click(getByText(/start/i))
  clearableNodes = getAllByText(/clear/i)
  expect(clearableNodes.length).toBeGreaterThan(1)

  // click clear on individual message
  fireEvent.click(clearableNodes[1])
  clearableNodes = getAllByText(/clear/i)
  expect(clearableNodes.length).toEqual(1)
})
