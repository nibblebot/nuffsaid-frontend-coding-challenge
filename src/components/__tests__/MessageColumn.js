import React from "react"
import { fireEvent, render } from "@testing-library/react"
import MessageColumn from "../MessageColumn"

it("should render MessageColumn", () => {
  const messages = [
    {
      id: 1,
      priority: 1,
      message: "this is an error message",
    },
    {
      id: 2,
      priority: 1,
      message: "this is a second error message",
    },
  ]
  const heading = "Error Type 1"
  const clearMessage = jest.fn()
  const { getByText, getAllByText } = render(
    <MessageColumn
      messages={messages}
      heading={heading}
      clearMessage={clearMessage}
    />
  )
  expect(getByText(heading)).toBeVisible()
  expect(getByText(/count 2/i)).toBeVisible()
  expect(getByText(messages[0].message)).toBeVisible()
  expect(getByText(messages[1].message)).toBeVisible()

  fireEvent.click(getAllByText(/clear/i)[0])
  fireEvent.click(getAllByText(/clear/i)[1])
  expect(clearMessage).toHaveBeenCalledTimes(2)
})
