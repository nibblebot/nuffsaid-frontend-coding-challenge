import React from "react"
import { fireEvent, render } from "@testing-library/react"

import MessageSnackbar from "../MessageSnackbar"

it("should render snackbar, close snackbar", () => {
  const message = "this is an error message"
  const setMessage = jest.fn()
  const { getByText, getByLabelText } = render(
    <MessageSnackbar message={message} setMessage={setMessage} />
  )
  expect(getByText(message)).toBeVisible()

  fireEvent.click(getByLabelText(/close/i))
  expect(setMessage).toHaveBeenCalledTimes(1)
})

it("should keep snackbar open on outside click", () => {
  const message = "this is an error message"
  const setMessage = jest.fn()
  const { getByText } = render(
    <div>
      <div>outside</div>
      <MessageSnackbar message={message} setMessage={setMessage} />
    </div>
  )

  fireEvent.click(getByText(/outside/i))
  expect(setMessage).toHaveBeenCalledTimes(0)
})
