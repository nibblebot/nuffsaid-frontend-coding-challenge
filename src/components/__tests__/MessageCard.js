import React from "react"
import { fireEvent, render } from "@testing-library/react"
import MessageCard from "../MessageCard"

it("should render MessageCard and click clear", () => {
  const onClick = jest.fn()
  const message = {
    id: 1,
    priority: 1,
    message: "this is an error message",
  }
  const { getByText } = render(
    <MessageCard message={message} onClick={onClick} />
  )
  expect(getByText(message.message)).toBeVisible()

  fireEvent.click(getByText(/clear/i))
  expect(onClick).toHaveBeenCalledTimes(1)
})
