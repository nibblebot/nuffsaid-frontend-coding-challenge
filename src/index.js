import React from "react"
import ReactDOM from "react-dom"
import MessageTable from "./components/MessageGrid"

const NewApp = require("./components/MessageGrid").default

function renderApp(App) {
  ReactDOM.render(<App />, document.getElementById("root"))
}

renderApp(MessageTable)

if (module.hot) {
  module.hot.accept("./components/MessageGrid", () => {
    renderApp(NewApp)
  })
}
